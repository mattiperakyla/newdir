# newdir

`newdir` is equivalent to `mkdir -p` except the last component of a directory
pathname must not exist.

    $ newdir path/to/
    $ newdir path/to/dir
    $ newdir path/to/dir
    newdir: Could not create the directory 'path/to/dir': File exists

This is usually what I want when I use the shell interactively. The last
command would have succeeded if I had used `mkdir -p` instead.

## newdirat

`newdirat` is like `newdir` except other directories are created relative to
the first directory.

    $ newdirat recipes sweet savory
    $ ls recipes
    savory  sweet

`newdirat` is the same binary as `newdir`.

## How to build newdir

`newdir` uses my support library, `libmatti`. The makefile expects to find the
library in the `libmatti` directory.

Other than that, just run `make`.

## Copyright

Copyright © 2022 Matti Peräkylä <mattiperakyla@mailbox.org>

This program is made available under the terms of the Eclipse Public License
2.0. See `LICENSE` for details.
