/* SPDX-FileCopyrightText: (c) 2022 Matti Perakyla <mattiperakyla@mailbox.org>
 * SPDX-License-Identifier: EPL-2.0 */

#define _XOPEN_SOURCE 700

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h> /* basename */
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include <matti.h>

static int create_directory(int dir, const char *path, int *created_dir,
		const char *relative_to_path);

static void print_usage(void);

int
main(int argc, char **argv)
{
	int status = 0;

	program_name = argv[0];

	if (argc < 2) {
		print_usage();
		return 2;
	}

	/* TODO: The 'basename' here is temporary - we do not want to modify
	 * 'argv[0]' because 'do_error' uses it. */
	if (strcmp(basename(argv[0]), "newdirat") == 0) {
		int dir;

		if (create_directory(AT_FDCWD, argv[1], &dir, NULL) == -1) {
			return 1;
		}

		for (int i = 2; i < argc; i++) {
			if (create_directory(dir, argv[i], NULL, argv[1])
					== -1) {
				status = 1;
			}
		}

		close(dir);
	} else {
		for (int i = 1; i < argc; i++) {
			if (create_directory(AT_FDCWD, argv[i], NULL, NULL)
					== -1) {
				status = 1;
			}
		}
	}

	return status;
}

static int
create_directory(int dir, const char *path, int *created_dir,
		const char *relative_to_path)
{
	size_t index;
	mode_t mode = S_IRWXU | S_IRWXG | S_IRWXO;

	/* TODO: The utility does not handle the pathnames "", "/", and "//"
	 * very elegantly. */

	if (path[0] == '/') {
		if (path[1] == '/' && path[2] != '/') {
			dir = openat(dir, "//", O_RDONLY);

			if (dir == -1) {
				do_error(0, errno, L"Could not open the "
						"directory '//'");
				return -1;
			}

			index = 2;
		} else {
			dir = openat(dir, "/", O_RDONLY);

			if (dir == -1) {
				do_error(1, errno, L"Could not open the root "
						"directory");
			}

			/* Skip leading forward slashes. */
			index = strspn(path, "/");
		}
	} else {
		bool is_cwd = dir == AT_FDCWD;

		dir = openat(dir, ".", O_RDONLY);

		if (dir == -1) {
			if (is_cwd) {
				do_error(1, errno, L"Could not open the "
						"working directory");
			} else {
				assert(relative_to_path != NULL);
				do_error(1, errno, L"Could not open the "
						"directory '%s'",
						relative_to_path);
			}
		}

		index = 0;
	}

	for (;;) {
		int new_dir;
		char *separator;

		separator = strchr(path + index, '/');

		if (separator == NULL || separator[strspn(separator, "/")]
				== '\0') {
			/* Gain a little consistency in error messages by
			 * stripping the trailing forward slashes from the
			 * filename, as other pathnames do so also. */

			if (separator != NULL) {
				*separator = '\0';
			}

			break;
		}
		
		*separator = '\0';

		while ((new_dir = openat(dir, path + index, O_RDONLY)) == -1) {
			if (errno != ENOENT) {
				do_error(0, errno, L"Could not open the "
						"directory '%s'", path);
				close(dir);
				return -1;
			}

			if (mkdirat(dir, path + index, mode) == -1
					&& errno != EEXIST) {
				do_error(0, errno, L"Could not create the "
						"directory '%s'", path);
				close(dir);
				return -1;
			}
		}

		close(dir);
		dir = new_dir;
		index += strlen(path + index);
		*separator = '/';
		index += strspn(separator, "/");
	}

	if (mkdirat(dir, path + index, mode) == -1) {
		do_error(0, errno, L"Could not create the directory '%s'",
				path);
		close(dir);
		return -1;
	}

	if (created_dir != NULL) {
		*created_dir = openat(dir, path + index, O_RDONLY);

		if (*created_dir == -1) {
			do_error(0, errno, L"Could not open the directory "
					"'%s'", path);
			close(dir);
			return -1;
		}
	}

	close(dir);
	return 0;
}

static void
print_usage(void)
{
	fwprintf(stderr, L"Usage: %s <directory> ...\n", program_name);
}
